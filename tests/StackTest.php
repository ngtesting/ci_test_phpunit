<?php
use PHPUnit\Framework\TestCase;

class StackTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     */
    public function testAdd($a, $b, $expected)
    {
        $this->assertEquals($expected, $a + $b);
    }

    /**
     *@test
     */
    public function withAnnotation()
    {
        $this->assertEquals(1, 1);
    }

    public function additionProvider()
    {
        return [
            [0, 0, 0],
            [1, 1, 2]
        ];
    }
}
?>